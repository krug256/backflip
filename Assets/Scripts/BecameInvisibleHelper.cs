﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BecameInvisibleHelper : MonoBehaviour
{

    public event EventHandler OnInvisible;

    private void OnBecameInvisible()
    {
        OnInvisible(this, new  EventArgs());

    }
}
