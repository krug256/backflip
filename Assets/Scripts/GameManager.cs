﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[DefaultExecutionOrder(-1)]
public class GameManager : MonoBehaviour
{
    [SerializeField] Player player;
    [SerializeField] Transform jumpDisk;
    [SerializeField] UI uI;
    public JumpSettings [] jumpSettings;

    int score;

    int settingsNmbr=0 ;


    void  AddScore(object sender,EventArgs eventArgs)
   {
        score++;
      
       StartCoroutine( SetSettings());
   }

    private void Update()
    {
        uI.DisplayScore(score);

        #region Test
        if (Input.GetKeyDown(KeyCode.D))
        {
            PlayerPrefs.DeleteAll();
            score = 0;
            Save();


        }
        if (Input.GetKeyDown(KeyCode.S))
        {
            Save();
        }
        if (Input.GetKeyDown(KeyCode.L))
        {
            Load();
        }
        #endregion
    }

    private void Awake()
    {
        Load();
        player.OnJump += AddScore;
    }

    IEnumerator SetSettings()
    {
         yield return new WaitForSeconds(0.4f);

        settingsNmbr++;
        if (settingsNmbr >= jumpSettings.Length)
        {
            settingsNmbr = 0;
        }
        player.JumpHeight = jumpSettings[settingsNmbr].JumpHight;
        jumpDisk.transform.localScale = new Vector3
            (
            jumpSettings[settingsNmbr].DiskScale, 
            jumpDisk.transform.localScale.y, 
            jumpSettings[settingsNmbr].DiskScale
            );
    }

    void Save()
    {
        PlayerPrefs.SetInt("score",score);
    }

    void Load()
    {
       score =  PlayerPrefs.GetInt("score",score);
    }


    private void OnApplicationPause(bool pause)
    {
        Save();
    }

    private void OnApplicationQuit()
    {
        Save();
    }

    
}
