﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "JumpSetUp", menuName = "Backflip/JumpSetting")]
public class JumpSettings : ScriptableObject
{
    [SerializeField] float jumpHight;
    [SerializeField] float diskScale;

    public float JumpHight { get { return jumpHight;  } private set { } }
    public float DiskScale { get { return diskScale; } private set { } }

}
