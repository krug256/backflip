﻿#define Debug
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    const float k_GroundedRadius = .1f;
  
    [SerializeField] LayerMask whatIsGround;
    [SerializeField] BecameInvisibleHelper bInvisHelper;
    [SerializeField] bool grounded = true;
    [SerializeField] bool jump;

    private Vector3 mousePos;

    public float JumpSpeed = 10f;
    public float MoveSpeed = 5f;
    public float JumpHeight = 15;


    private Animator animator;
  
    private Vector3 startJumpPos;
    private Vector3 betweenJumpPos;
    private Vector3 endJumpPos;
    private float time;
    private float[] speedsByChordsLengths;
    private float totalLength;


    private Vector3 startPosition;

    public void ResetPosition(object sender, EventArgs eventArgs )
    {
        transform.position = startPosition;
    }

    public event EventHandler OnJump;

    private void Awake()
    {
        bInvisHelper.OnInvisible += ResetPosition;

        startPosition = transform.position;
        animator = GetComponent<Animator>();


    }

    void Move()
    {
        //Fly
        if (jump&&!grounded) 
        {
            time += Time.deltaTime * JumpSpeed / totalLength / GetSpeedByCoordLength(time);
            time = Mathf.Clamp01(time);

            Vector3 b = GetQuadBezierPoint(startJumpPos, betweenJumpPos, endJumpPos, time);

            transform.position = b;
           
            
        }
        //Fall
        else if (!grounded && !jump)
        {
            transform.Translate(Vector3.down);
            time = 0;
        }
        //move right
        else if (grounded && !jump)
        {
            print("move");
             transform.Translate(Vector3.right*Time.deltaTime* MoveSpeed);
            time = 0;
        }
        else
        {
            time =0;

        }
        animator.SetFloat("Blend", time);
    }

    void  CheckGround()
    {
        grounded = false;
        Collider[] colliders = Physics.OverlapSphere(transform.position, k_GroundedRadius, whatIsGround);
        for (int i = 0; i < colliders.Length; i++)
        {
            if (colliders[i].gameObject != gameObject && colliders[i].gameObject.tag != "disk")
            {
                grounded = true;
                jump = false;

            }


        }

    }
    
    void Jump()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, whatIsGround))
        {
            mousePos.x = hit.point.x;
            mousePos.y = 0;
            mousePos.z = 0;

            if (mousePos.x < transform.position.x && Vector3.Distance(mousePos, transform.position) > 2f)
            {
                startJumpPos = transform.position;
                endJumpPos = mousePos;
                betweenJumpPos = new Vector3((endJumpPos.x + startJumpPos.x) / 2, JumpHeight - startJumpPos.y, 0);

                PrepareCoords();

                grounded = false;
                jump = true;
                OnJump(this, new EventArgs());

            }

        }
    }

    private void OnTriggerEnter(Collider other)
    {

        if (other)
        {
            readyToJump = true;

        }
    }

    private void OnTriggerExit(Collider other)
    {
        readyToJump = false;
    }

    bool readyToJump = false;



    void Update()
    {
        CheckGround();

        if (Input.GetMouseButtonDown(0)&& readyToJump)
        {
            
            Jump();        
        }

        animator.SetBool("StartRotate",jump);

        Move();
#if Debug
        DrawPoints();
#endif
    }

    private int subdivs = 60;
    Vector3 GetQuadBezierPoint(Vector3 p0, Vector3 p1, Vector3 p2, float t)
    {
        float invT = 1 - t;
        return (invT * invT * p0) + (2 * invT * t * p1) + (t * t * p2);
    }

    private void PrepareCoords()
    {
        speedsByChordsLengths = new float[subdivs];

        Vector3 prevPos = startJumpPos;
        for (int i = 0; i < subdivs; i++)
        {
            Vector3 curPos = GetQuadBezierPoint(startJumpPos, betweenJumpPos, endJumpPos, (i + 1) / (float)subdivs);
            float length = Vector3.Magnitude(curPos - prevPos);

#if Debug
            Debug.DrawLine(curPos, prevPos, Color.yellow, 30f);
#endif

            speedsByChordsLengths[i] = length;
            totalLength += length;
            prevPos = curPos;
        }

        for (int i = 0; i < subdivs; i++)
        {
            speedsByChordsLengths[i] = speedsByChordsLengths[i] / totalLength * subdivs;
        }
    }


    float GetSpeedByCoordLength(float t)
    {
        int pos = (int)(t * subdivs) - 1;
        pos = Mathf.Clamp(pos, 0, subdivs - 1);
        return speedsByChordsLengths[pos];
    }

    private void OnDisable()
    {
        bInvisHelper.OnInvisible -= ResetPosition;
    }



    #region Debug
    
#if Debug
    private Vector3 _prevPos;
    private List<Vector3> _testPositions = new List<Vector3>();
#endif
   
#if Debug
    void DrawPoint(Vector3 p)
    {
        Debug.DrawRay(p + Vector3.down * 0.5f * 0.1f, Vector3.up * 0.1f);
        Debug.DrawRay(p + -Vector3.forward * 0.5f * 0.1f, Vector3.forward * 0.1f);
    }

    void DrawPoints()
    {
        _testPositions.Add(transform.position);

        foreach (Vector3 t in _testPositions)
        {
            DrawPoint(t);
        }
    }
#endif

     
    #endregion



}
