﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI : MonoBehaviour
{
    [SerializeField] Text score;

    public void DisplayScore(int number )
    {
        score.text = "Score " + number;
    }
   

}
